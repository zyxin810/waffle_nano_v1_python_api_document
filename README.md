# Waffle Nano V1 MicroPython 接口文档（Waffle Nano MicroPython API docs）

<a href='https://gitee.com/blackwalnutlabs/waffle_nano_v1_python_api_document/stargazers'><img src='https://gitee.com/blackwalnutlabs/waffle_nano_v1_python_api_document/badge/star.svg?theme=dark' alt='star'></img></a>

欢迎! 您现在浏览的是 `Waffle Nano Python` 接口文档（中文），由 `黑胡桃实验室-OpenHarmony` 团队维护更新。 

## Waffle Nano 开发套件简介

华夫饼鸿蒙版（Waffle Nano）是一款黑胡桃实验室在研究 OpenHarmony 开源项目中制作出来的一款 OpenHarmony 创客套件。

- 套件支持 Python 和 C 两种语言开发应用程序；
- 套件配置云端编程平台，无需在本地配置开发环境，即可编写鸿蒙应用；
- 套件中的开发板部分包含 3 轴加速度计、3 轴陀螺仪、3 轴磁力计、NFC Tag 芯片和 240*240 TFT 屏幕；
- 套件中的开发板部分主控为WiFi SOC Hi3861，160MHz主频，SRAM 352KB、ROM 288KB、2M Flash；
- 套件通过 TypeC 接口实现供电、连接平台、下载程序、调试程序等多种功能。

## 快速上手 Python 开发鸿蒙应用

如果您想快速的体验一下 OpenHarmony 使用 Python 开发应用，无需配置开发环境，只需要使用以下两步：

1、开箱套件

按照开箱视频打开硬件，并安装外壳。[开箱视频](https://www.bilibili.com/video/BV1j3411r7N8)

2、连接 IDE 开始编写代码。[演示视频](https://www.bilibili.com/video/BV1n64y1s7SW)

> 目前 OpenHarmony 在持续升级中，建议及时更新固件以获得更好的 Python 开发体验。[固件下载页面](https://wafflenano.blackwalnut.tech/#/firmware)

## Python API 接口目录

| 模块 | 说明 |
|---|---|
|[base](base/README.md)| 支持的 python3 语法 |
|[builtin](builtin/README.md)| 内置方法，比如异常处理、列表、字符串等 |
|[GPIO](machine/GPIO/README.md)| machine.GPIO 库 |
|[UART](machine/UART/README.md)| machine.UART  库 |
|[I2C](machine/I2C/README.md)| machine.I2C  库 |
|[SPI](machine/SPI/README.md)| machine.SPI  库 |
|[PWM](machine/PWM/README.md)| machine.PWM  库 |
|[ADC](machine/ADC/README.md)| machine.ADC  库 |
|[math](math/README.md)| 常用数学函数 |
|[memoryview](memoryview/README.md)| 内存视图相关操作 |
|[network](network/README.md)| 网络相关接口 |
|[socket](socket/README.md)| socket 接口是网络通信的基础 |
|[timer](timer/README.md)| 定时器 |
|[ubinascii](ubinascii/README.md)| 二进制数据和ASCII格式的各种编码之间的转换（双向） |
|[ucollections](ucollections/README.md)| 高级集合和容器类型来保存/累积各种对象 |
|[uctypes](uctypes/README.md)| 自定义数据结构 |
|[uheapq](uheapq/README.md)| 堆队列算法 |
|[uhashlib](uhashlib/README.md)| SHA256算法加密安全算法 |
|[ujson](ujson/README.md)| JSON数据格式 |
|[urandom](urandom/README.md)| 生成随机数 |
|[ure](ure/README.md)| 正则表达式相关操作 |
|[ustruct](ustruct/README.md)| 打包和解压缩数据 |
|[utime](utime/README.md)| 提供用于获取当前时间和日期、测量时间间隔和延迟的函数 |
|[gc](gc/README.md)| 内存管理 |
|[uos](uos/README.md)| 文件管理 |
|[BME280](bme280/README.md)| BME280 驱动程序 |
|[ICM20948](icm20948/README.md)| ICM20948 驱动程序 |
|[ST7789](st7789/README.md)| ST7789 驱动程序 |
|[framebuf](framebuf/README.md)| 提供可用于创建位图、可发送到显示器的通用帧缓冲区操作库 |
|[nfc](nfc/README.md)| NFC 调用接口 |
|[QRCode](QRCode/README.md)| 二维码生成库 |


## 外部链接

[Waffle Nano 创客套件](https://wafflenano.blackwalnut.tech/)

[Waffle Nano 固件下载](https://wafflenano.blackwalnut.tech/#/firmware)

[Waffle Nano 硬件获取](https://item.taobao.com/item.htm?spm=a230r.1.14.16.7633252853ro5G&id=649758897293)

[Waffle Nano 传感器库](https://gitee.com/blackwalnutlabs/waffle-nano-v1-sensor-lib)

[黑胡桃实验室官网](https://blackwalnut.tech/)

[OpenHarmony 开源项目](https://gitee.com/openharmony)

## 关于我们

![黑胡桃实验室公众号](assets/bwqrcode.jpg)

## 参与贡献

1.  `Fork` 本仓库
2.  新建 `Feat_xxx` 分支
3.  提交代码
4.  新建 `Pull Request`