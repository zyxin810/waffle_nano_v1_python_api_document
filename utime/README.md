# utime 时间相关的函数

  - [概要](#概要)
    - [维护实际日历日期/时间](#维护实际日历日期时间)
  - [Utime API详解](#utime-api详解)
    - [函数](#函数)
      - [时间元组](#时间元组)
      - [时间元组逆函数](#时间元组逆函数)
      - [休眠时间](#休眠时间)
      - [递增计数器](#递增计数器)
      - [递增计数器间隔计算](#递增计数器间隔计算)
      - [开始时间的秒数](#开始时间的秒数)
  - [示例](#示例)

## 概要

&emsp;&emsp; 本节介绍时间函数utime概念，以及如何在MicroPython中使用utime。

&emsp;&emsp;`utime` 模块提供用于获取当前时间和日期、测量时间间隔和延迟的函数。

### 维护实际日历日期/时间

- **时刻**: 使用2000-01-01 00:00:00 UTC开始的时间。
- **维护实际日历日期/时间**: 这需要实时通信（RTC）。在具备底层OS的系统中，RTC可能是隐式的。设置和维护实际日历时间应是OS/RTOS的职能， 且在MicroPython之外完成，只使用OS API查询日期/时间。

## Utime API详解

&emsp;&emsp;使用`import utime`导入定时器`utime`模块

&emsp;&emsp;再使用`TAB` 按键来查看`utime`中所包含的内容：

```python
>>> import utime
>>> utime.
__name__        gmtime          localtime       mktime
sleep           sleep_ms        sleep_us        ticks_add
ticks_cpu       ticks_diff      ticks_ms        ticks_us
time
```

### 函数

#### 时间元组

&emsp;&emsp;`utime.localtime([secs])`

&emsp;&emsp;函数说明:将一个以秒计的时间转换为一个包含下列内容的8元组：（年、月、日、小时、分钟、秒、一周中某日、一年中某日）。若未提供秒或为None，则使用2000年1月1日以来的时间8元组

- 年包括世纪（例如2014）
- 月为 1-12
- 日为 1-31
- 小时为 0-23
- 分钟为 0-59
- 秒钟 0-59
- 周中某日为 0-6 （对应周一到周日）
- 年中某日为 1-366

&emsp;&emsp;示例：

```python
>>> import utime
>>> utime.localtime(1000)#转换1000s为时间元组
(2000, 1, 1, 0, 16, 40, 5, 1)
```

#### 时间元组逆函数

&emsp;&emsp;`utime.mktime([tuple])`

&emsp;&emsp;函数说明：局部时间的逆函数，其参数为一个表示本地时间的8元组。返回一个表示2000年1月1日以来的秒钟的整数。

&emsp;&emsp;示例：

```python
>>> import utime
>>> utime.mktime([2000,1,1,0,0,0,5,1])#将2000年1月1日0时0分0秒周六，一年中第一天转换成2000年1月1日以来的秒钟的整数
0
```

#### 休眠时间

&emsp;&emsp;`utime.sleep(seconds)`

&emsp;&emsp;函数说明：休眠给定秒数的时间。秒钟数可为一个表示休眠时间的浮点数。注意：其他端口可能不接受浮点参数，为满足兼容性，使用 `sleep_ms()` 和 `sleep_us()` 函数。

&emsp;&emsp;示例：

```python
>>> import utime
>>> utime.sleep(2)#休眠2秒，等2s后才可以干其他事
```

&emsp;&emsp;`utime.sleep_ms(*ms*)`

&emsp;&emsp;函数说明：休眠给定毫秒数的时间，应为正整数或0，1s=1000ms。

&emsp;&emsp;示例：

```python
>>> import utime
>>> utime.sleep_ms(200)#休眠0.2秒，等0.2s后才可以干其他事
```

&emsp;&emsp;`utime.sleep_us(*us*)`

&emsp;&emsp;函数说明：休眠给定微秒数的时间。应为正整数或0，1s=1000ms=1000000us。

&emsp;&emsp;示例：

```python
>>> import utime
>>> utime.sleep_us(2000000)#休眠0.2秒，等0.2s后才可以干其他事
```

#### 递增计数器

&emsp;&emsp;`utime.ticks_ms()`

&emsp;&emsp;函数说明：返回不断递增的毫秒计数器，在某些值后会重新计数(未指定)。计数值本身无特定意义，只适合用在 `utime.ticks_diff()`和`utime.ticks_add()`

&emsp;&emsp;`utime.ticks_us()`

&emsp;&emsp;函数说明：返回不断递增的微秒计数器，在某些值后会重新计数(未指定)。计数值本身无特定意义，只适合用在 `utime.ticks_diff()`和`utime.ticks_add()`

&emsp;&emsp;`utime.ticks_cpu()`

&emsp;&emsp;函数说明：与 `ticks_ms` 和 `ticks_us` 相似，但有更高的分辨率（通常CPU时钟）。计数值本身无特定意义，只适合用在 `utime.ticks_diff()`和`utime.ticks_add()`

#### 递增计数器间隔计算

&emsp;&emsp;`utime.ticks_add(ticks, delta)`

&emsp;&emsp;函数说明：用一个给定数字来抵消ticks值，该数字可为正或负。给定一个 *ticks* 值，该函数允许计算之前或之后ticks的值，返回一个表示2000年1月1日以来的秒钟的整数

&emsp;&emsp;`ticks`：须为调用的 `utime.ticks_ms()` 、 `utime.ticks_us()`、 `utime.ticks_cpu()` 函数

&emsp;&emsp;`delta`：给定的毫秒数值，为负数代表之前，为正数代表之后。

&emsp;&emsp;示例：

```python
>>> import utime
>>> utime.ticks_add(utime.ticks_ms,-100)#找到100ms前的计数器值
452284
```

&emsp;&emsp;`utime.ticks_diff(new, old)`

&emsp;&emsp;函数说明：测量连续调用调用 `utime.ticks_ms()` 、 `utime.ticks_us()`、 `utime.ticks_cpu()` 函数间周期。由这些函数返回的值可能在任何时间停止，因此并不支持直接减去这些值。

&emsp;&emsp;`old`：旧时间，须为调用的 `utime.ticks_ms()` 、 `utime.ticks_us()`、 `utime.ticks_cpu()` 函数

&emsp;&emsp;`new`：新时间，须为调用的 `utime.ticks_ms()` 、 `utime.ticks_us()`、 `utime.ticks_cpu()` 函数。

&emsp;&emsp;“旧” 时间需要在 “新” 时间之前，否则结果无法确定。这个函数不要用在计算很长的时间 (因为 ticks_*() 函数会回绕，通常周期不是很长)。

&emsp;&emsp;示例：

```python
>>> import utime
>>> start = utime.ticks_us()#开始时间为微秒的递增计数器
>>> utime.ticks_diff(utime.ticks_us(),start)#测量开始时间到新的微秒的递增计数器开始时的差值。
220003042
```

#### 开始时间的秒数

&emsp;&emsp;`utime.time`()

&emsp;&emsp;函数说明：返回从开始时间的秒数（整数），假设 RTC 已经设置好。如果 RTC 没有设置，函数将返回参考点开始计算的秒数。开发可移植的MicroPython应用程序， 不应依赖该函数来提供高于第二精度的结果。若需要更高精度，需要使用 `ticks_ms()` 和 `ticks_us()` 函数。 需要日历时间，无参数的 `localtime()` 不失为佳选。

&emsp;&emsp;示例：

```python
>>> import utime
>>> utime.time()#返回参考点开始的秒数
15394
```

## 示例

&emsp;&emsp;显示各种类型时间

```python
import utime
set_time = utime.mktime((2021, 5, 8, 11, 30, 0, 0, 27))
print(set_time)
print(utime.localtime(set_time))
print(utime.localtime())
print(utime.mktime(utime.localtime()))
print(utime.localtime(utime.ticks_add(utime.ticks_ms,-100)))
```

&emsp;&emsp;第一行导入时间`utime`

&emsp;&emsp;第二、三行把8个元组形式的2000年1月1日到给定时间转换成秒数的整数并打印。

&emsp;&emsp;第四行把秒数的整数转换成8元组形式的时间格式

&emsp;&emsp;第五行：utime.localtime()输出2000年1月1日以来的时间8元组

&emsp;&emsp;第六行：utime.localtime()输出2000年1月1日以来的时间8元组，utime.mktime()需要的参数为时间的8元组，所以会打印出2000年1月1日以来的时间的秒数的整数

&emsp;&emsp;第七行：utime.ticks_add(ticks,delta)会计算给定一个 *ticks* 值之前或之后时间秒数的整数，*ticks* 值是返回不断递增的毫秒计数器utime.ticks_ms，delta是-100ms，因为其是负数，所以是找100ms前的计数器值的秒数的正整数。再通过utime.localtime(）将其转换成8元组类型时间格式打印出来。

