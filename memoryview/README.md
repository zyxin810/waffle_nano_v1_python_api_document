# memoryview（）

  - [概要](#概要)
    - [Python缓冲区协议](#python缓冲区协议)
    - [什么是内存视图？](#什么是内存视图)
    - [为什么缓冲区协议和内存视图很重要？](#为什么缓冲区协议和内存视图很重要)
  - [memoryview 详解](#memoryview-详解)
    - [memoryview（）参数](#memoryview参数)
    - [从memoryview（）返回值](#从memoryview返回值)
  - [示例](#示例)

## 概要

&emsp;&emsp;`memoryview()`函数返回给定参数的内存视图对象。

&emsp;&emsp;在了解什么是内存视图之前，需要首先了解什么是缓冲区协议。

### Python缓冲区协议

&emsp;&emsp;缓冲区协议提供了一种访问对象内部数据的方法。该内部数据是存储阵列或缓冲区。

&emsp;&emsp;缓冲区协议允许一个对象公开其内部数据（缓冲区），而另一个对象无需中间复制即可访问这些缓冲区。

&emsp;&emsp;只能在C-API级别上访问此协议，而不能使用常规代码库。

&emsp;&emsp;因此，为了将相同的协议公开给普通的Python代码库，需要使用内存视图。

### 什么是内存视图？

&emsp;&emsp;内存视图是在Python中公开缓冲区协议的安全方法。

&emsp;&emsp;允许通过创建内存视图对象来访问对象的内部缓冲区。

### 为什么缓冲区协议和内存视图很重要？

&emsp;&emsp;每当对对象执行某些操作（调用对象的函数，对数组进行切片）时，Python都需要创建**该对象**的**副本**。

&emsp;&emsp;处理大量数据（例如，图像的二进制数据），则不必要地创建大量数据的副本，这几乎毫无用处。

&emsp;&emsp;使用缓冲协议，可以给另一个对象访问权限以使用/修改大数据，而无需复制它。这使程序使用更少的内存并提高了执行速度。

## memoryview 详解

&emsp;&emsp;要使用公开缓冲区协议`memoryview()`，使用以下语法：

```python
memoryview(obj)
```

### memoryview（）参数

&emsp;&emsp;`memoryview()`函数采用单个参数：

- **obj-**要公开其内部数据的对象。`obj`必须支持缓冲协议（bytes，bytearray）。
  示例：
  ```python
  >>> memoryview(b'\x00')
  <memoryview>
   ```

### 从memoryview（）返回值

&emsp;&emsp;`memoryview()`函数返回一个内存视图对象。

## 示例

&emsp;&emsp;使用memoryview（）

```python
random_byte_array = bytearray('ABC', 'utf-8')
mv = memoryview(random_byte_array)
print(mv[0])
print(bytes(mv[0:2]))
print(list(mv[0:3]))
```
&emsp;&emsp;第一行创建utf8格式的bytearray对象ABC

&emsp;&emsp;第二行创建了一个内存视图对象 MV 

&emsp;&emsp;第三行访问了 MV的第0个索引，'A'并打印出来（给出ASCII值-65）

&emsp;&emsp;第四行访问了 MV的索引从0和1开始'AB'，并将其转换为字节

&emsp;&emsp;第五行访问了 MV的索引从0和2开始'ABC'，并将其转换为字节