# `urandom` –随机数生成

  - [概要](#概要)
  - [`urandom` 详解](#urandom-详解)
    - [函数](#函数)

## 概要

&emsp;&emsp;模块 `urandom` 提供生成随机数的功能。

## `urandom` 详解

&emsp;&emsp;使用`import urandom`导入`urandom`类. 

### 函数

- `urandom.getrandbits`（*n* ）

  函数说明：返回带有`n`比特长的随机整数，其中n可以在1-32（含）之间。

  示例：
  ```python
  >>> import urandom
  >>> urandom.getrandbits(8)#返回8比特位的随机整数
  216
  ```

- `urandom.randint`（*a*，*b* ）

  函数说明：返回一个在`a <= N <= b`时的随机整数`N`，等同于`randrange(a, b+1)`

  示例：
  ```python
  >>> import urandom
  >>> urandom.randint(1,6)#返回1~6之间的随机整数
  3
  ```

- `urandom.seed`（*n* ）

  函数说明：用已知的整数`n`初始化随机数生成器。从给定起始状态（`n`）开始提供确定的随机性。

  示例：
  ```python
  >>> import urandom
  >>> urandom.seed(4)#提供确定的随机性
  >>> urandom.randint(1,6)#返回1~6之间的随机整数
  3
  >>> urandom.randint(1,6)#生成的随机数一样
  3
  ```

- `urandom.randrange`（*停止*）

  函数说明：返回介于 `0` 到最大值`stop`之间（但不包括）随机选择的数。

  示例：
  ```python
  >>> import urandom
  >>> urandom.randrange(5)#返回0-4之间随机选择的数
  3
  ```

- `urandom.randrange`（*开始*，*停止*）

  函数说明：返回一个`range(start, stop)` 随机选择的数。
  
  示例：
  ```python
  >>> import urandom
  >>> urandom.randrange(2,7)#返回2-6之间随机选择的数
  5
  ```

- `urandom.randrange`（*开始*，*停止*，*步长*）

  函数说明：返回一个 `range(start, stop, step)` 随机选择的数。
  
  示例：
  ```python
  >>> import urandom
  >>> urandom.randrange(1,9,3)#返回1-8之间间隔为3的随机数
  4
  >>> urandom.randrange(1,9,3)
  7
  ```

- `urandom.choice`（*seq* ）

  函数说明：从非空序列返回一个随机元素`seq`。如果`seq`为空，则引发`IndexError`。
  
  示例：
  ```python
  >>> import urandom
  >>> seq=[1,2,3,4,5]
  >>> urandom.choice(seq)#从序列seq返回一个随机元素
  4
  ```

- `urandom.random`（）

  函数说明：返回范围为[0.0，1.0）的下一个随机浮点数
  
  示例：
  ```python
  >>> import urandom
  >>> urandom.random()#返回范围为[0.0，1.0）的下一个随机浮点数
  0.003348340879060441
  ```

- `urandom.uniform`（*a*，*b* ）

  函数说明：返回一个随机浮点数`N`，满足 `a <= N <= b`且`a <= b`或`b <= N <= a`且`b < a`
  
  示例：
  ```python
  >>> import urandom
  >>> urandom.uniform(1,8)#返回1-8之间随机浮点数
  4.673889480152304
  ```