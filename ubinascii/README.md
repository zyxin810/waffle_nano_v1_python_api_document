# `ubinascii` – 二进制/ASCII转换

  - [概要](#概要)
  - [ubinascii API详解](#ubinascii-api详解)
    - [函数](#函数)

## 概要

&emsp;&emsp;该模块实现相应CPython模块的子集。

&emsp;&emsp;该模块实现二进制数据和ASCII格式的各种编码之间的转换（双向）。

## ubinascii API详解

&emsp;&emsp;使用`import ubinascii`导入`ubinascii`模块

&emsp;&emsp;再使用`TAB` 按键来查看`ubinascii`中所包含的内容：

```python
>>> import ubinascii
>>> ubinascii.
__name__        a2b_base64      b2a_base64      crc32
hexlify         unhexlify
```

### 函数

- `ubinasciihexlify`(*data*[, *sep*])

  将二进制数据转换为十六进制表示。返回字节字符串。Difference to CPython若给定附加参数，则提供 `sep` ，用做十六进制值的分隔符。

- `ubinasciiunhexlify`(*data*)

  将十六进制数据转换为二进制表示。返回字节字符串。

- `ubinasciia2b_base64`(*data*)

  将Base64编码数据转换为二进制表示。返回字节字符串。

- `ubinasciib2a_base64`(*data*)

  将二进制数据转换为Base64编码数据。返回字节字符串。
