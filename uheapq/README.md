# `uheapq` – 堆队列算法

  - [概要](#概要)
  - [`uheapq` API详解](#uheapq-api详解)
    - [函数](#函数)

## 概要

&emsp;&emsp;该模块实现相应`CPython`模块的子集

&emsp;&emsp;该模块实现堆队列算法。简而言之，堆队列即为以一定方式储存其所有项的列表。

## `uheapq` API详解

&emsp;&emsp;使用`import uheapq`导入`uheapq`模块

&emsp;&emsp;再使用`TAB` 按键来查看`uheapq`中所包含的内容：

```python
>>> import uheapq
>>> uheapq.
__name__        heapify         heappop         heappush
```

### 函数

- `uheapq.heappush`(*heap*, *item*)

  将 `item` 载入 `heap` 中。

  示例：
  
  ```python
  >>> buf=[1,2,3]
  >>> uheapq.heappush(buf,4)
  >>> buf
  [1, 2, 3, 4]
  ```

- `uheapq.heappop`(*heap*)

  从 `heap` 中提取首个项，并返回。若堆为空，则引发Index错误。

   示例：

  ```python
  >>> buf=[1,2,3]
  >>> uheapq.heappop(buf)
  1
  >>> buf
  [2, 3]
  ```

- `uheapq.heapify`(*x*)

  将列表 `x` 转换为一个堆。此为就地操作。

   示例：

  ```python
  >>> buf=[1,2,3]
  >>> uheapq.heapify(buf)
  >>> buf
  [1, 2, 3]
  ```
  
  