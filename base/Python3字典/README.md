## Python3字典

- [访问字典里的值](#访问字典里的值)
- [修改字典](#修改字典)
- [删除字典元素](#删除字典元素)
- [字典内置函数](#字典内置函数)



&emsp;&emsp;字典是另一种可变容器模型，且可存储任意类型对象。

&emsp;&emsp;字典的每个键值 `key=>value` 对用冒号 `:` 分割，每个对之间用逗号 `(,)` 分割，整个字典包括在花括号 `{}` 中 ,格式如下所示： 

```python
d = {key1 : value1, key2 : value2, key3 : value3 }
```


&emsp;&emsp;键必须是唯一的，但值则不必。

&emsp;&emsp;值可以取任何数据类型，但键必须是不可变的，如字符串，数字。

&emsp;&emsp;一个简单的字典实例：

```python
dict = {'name': 'Waffle', 'likes': 123}
```

&emsp;&emsp;也可如此创建字典：

```python
dict1 = { 'abc': 456 }
dict2 = { 'abc': 123, 98.6: 37 }
```
---

## 访问字典里的值

&emsp;&emsp;把相应的键放入到方括号中，如下实例:

```python
dict = {'Name': 'Waffle', 'Age': 7, 'Class': 'First'}
 
print ("dict['Name']: ", dict['Name'])
print ("dict['Age']: ", dict['Age'])
```

&emsp;&emsp;以上实例输出结果：

```shell
dict['Name']:  Waffle
dict['Age']:  7
```

&emsp;&emsp;如果用字典里没有的键访问数据，会输出错误如下：

```python
dict = {'Name': 'Waffle', 'Age': 7, 'Class': 'First'}
 
print ("dict['Alice']: ", dict['Alice'])
```

以上实例输出结果：

```python
Traceback (most recent call last):
  File "test.py", line 3, in <module>
    print ("dict['Alice']: ", dict['Alice'])
KeyError: 'Alice'
```

---

## 修改字典

&emsp;&emsp;向字典添加新内容的方法是增加新的键/值对，修改或删除已有键/值对如下实例:

```python
dict = {'Name': 'Waffle', 'Age': 7, 'Class': 'First'}
 
dict['Age'] = 8               # 更新 Age
dict['School'] = "黑胡桃实验室"  # 添加信息
 
 
print ("dict['Age']: ", dict['Age'])
print ("dict['School']: ", dict['School'])
```

以上实例输出结果： 

```shell
dict['Age']:  8
dict['School']:  黑胡桃实验室
```

---

## 删除字典元素

&emsp;&emsp;能删单一的元素也能清空字典，清空只需一项操作。

&emsp;&emsp;显示删除一个字典用 `del` 命令，如下实例：

```python
dict = {'Name': 'Waffle', 'Age': 7, 'Class': 'First'}
 
del dict['Name'] # 删除键 'Name'
dict.clear()     # 清空字典
del dict         # 删除字典
 
print ("dict['Age']: ", dict['Age'])
print ("dict['School']: ", dict['School'])
```

&emsp;&emsp;但这会引发一个异常，因为用执行 `del` 操作后字典不再存在：

```shell
Traceback (most recent call last):
  File "<stdin>", line 7, in <module>
TypeError: 'type' object isn't subscriptable
```

注：`del()` 方法后面也会讨论。

**字典键的特性**

&emsp;&emsp;字典值可以是任何的 python 对象，既可以是标准的对象，也可以是用户定义的，但键不行。

&emsp;&emsp;两个重要的点需要记住：

1）不允许同一个键出现两次。创建时如果同一个键被赋值两次，后一个值会被记住，如下实例：

```python
dict = {'Name': 'Waffle', 'Age': 7, 'Name': '黑胡桃'}
 
print ("dict['Name']: ", dict['Name'])
```

以上实例输出结果：

```shell
dict['Name']:  黑胡桃
```

2）键必须不可变，所以可以用数字，字符串或元组充当，而用列表就不行，如下实例：

```python
dict = {['Name']: 'Waffle', 'Age': 7}
 
print ("dict['Name']: ", dict['Name'])
```

以上实例输出结果：

```shell
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: unsupported type for __hash__: 'list'
```

---

## 字典内置函数

&emsp;&emsp;Python字典包含了以下内置函数：

| 序号 | 函数 | 描述 |
| --- | --- | --- |
| 1 | len(dict) | 计算字典元素个数，即键的总数。|
| 2 | str(dict) | 输出字典，以可打印的字符串表示。 |
| 3 | 	type(variable) | 返回输入的变量类型，如果变量是字典就返回字典类型。| 




