# Python3 基本数据类型

- [多个变量赋值](#多个变量赋值)
- [标准数据类型](#标准数据类型)
- [Number(数字)](#Number(数字))
- [数值运算](#数值运算)
- [String(字符串)](#String(字符串))
- [List(列表)](#List(列表))
- [Tuple(元组)](#Tuple(元组))
- [Set(集合)](#Set(集合))
- [Dictionary(字典)](#Dictionary(字典))
- [Python 数据类型转换](#Python数据类型转换)




&emsp;&emsp;`Python` 中的变量不需要声明。每个变量在使用前都必须赋值，变量赋值以后该变量才会被创建。

&emsp;&emsp;在 `Python` 中，**变量** 就是 **变量**，它没有 **类型**，我们所说的 **"类型"** 是变量所指的内存中对象的类型。

&emsp;&emsp;等号`（=）`用来给变量赋值。

&emsp;&emsp;等号`（=）`运算符左边是一个变量名,等号`（=）`运算符右边是存储在变量中的值。例如：

```python
counter = 100          # 整型变量
miles   = 1000.0       # 浮点型变量
name    = "Waffle Nano"     # 字符串

print (counter)
print (miles)
print (name)
```

执行以上代码，输出结果为：

<center><img src="assets/image/1.png" width="500"/></center>

<center>(变量赋值)</center>

&emsp;

---

## 多个变量赋值

&emsp;&emsp;`Python` 允许你同时为多个变量赋值。例如：

```python
a = b = c = 1
```

&emsp;&emsp;以上实例，创建一个整型对象，值为 `1`，从后向前赋值，三个变量被赋予相同的数值。

&emsp;&emsp;您也可以为多个对象指定多个变量。例如：

```python
a, b, c = 1, 2.5, "Waffle"
```

&emsp;&emsp;以上实例，两个整型对象 `1` 和 `2.5` 的分配给变量 `a` 和 `b`，字符串对象 `"Waffle"` 分配给变量 `c`。

---

## 标准数据类型

&emsp;&emsp;`Python3` 中有六个标准的数据类型：

- `Number`(数字)
- `String`(字符串)
- `List`(列表)
- `Tuple`(元组)
- `Set`(集合)
- `Dictionary`(字典)

&emsp;&emsp;`Python3` 的六个标准数据类型中：

- **不可变数据（3 个）**：`Number`（数字）、`String`（字符串）、`Tuple`（元组）；
- **可变数据（3 个）**：`List`（列表）、`Dictionary`（字典）、`Set`（集合）。 

---

## Number(数字)

&emsp;&emsp;`Python3` 支持 `int`、`float`、`bool`、`complex`（复数）。

&emsp;&emsp;在 `Python 3` 里，只有一种整数类型 `int`，表示为长整型，没有 `python2` 中的 `Long`。

&emsp;&emsp;像大多数语言一样，数值类型的赋值和计算都是很直观的。

&emsp;&emsp;内置的 `type()` 函数可以用来查询变量所指的对象类型。

```shell
>>> a, b, c, d = 20, 5.5, True, 4+3j
>>> print(type(a), type(b), type(c), type(d))
<class 'int'> <class 'float'> <class 'bool'> <class 'complex'>
```

&emsp;&emsp;此外还可以用 `isinstance` 来判断：

```shell
>>> a = 111
>>> isinstance(a, int)
True
>>>
```

>**注意**：`Python3` 中，`bool `是 `int` 的子类，`True` 和 `False` 可以和数字相加 `True==1`，`False==0` 是会返回 `Ture`，但可以通过 `is` 来判断类型。

```shell
>>> issubclass(bool, int) 
True
>>> True==1
True
>>> False==0
True
>>> True+1
2
>>> False+1
1
>>> 1 is True
False
>>> 0 is False
False
```
>在 `Python2 `中是没有布尔型的，它用数字 `0` 表示` False`，用 `1` 表示 `True`。

&emsp;&emsp;当你指定一个值时，`Number` 对象就会被创建：

```python
var1 = 1
var2 = 10
```

&emsp;&emsp;您也可以使用 `del` 语句删除一些对象引用。

&emsp;&emsp;`del` 语句的语法是：

```python
del var1[,var2[,var3[....,varN]]]
```

&emsp;&emsp;您可以通过使用 `del` 语句删除单个或多个对象。例如：

```python
del var
del var_a, var_b
```

---

## 数值运算

```shell
>>> 5 + 4  # 加法
9
>>> 4.3 - 2 # 减法
2.3
>>> 3 * 7  # 乘法
21
>>> 2 / 4  # 除法，得到一个浮点数
0.5
>>> 2 // 4 # 除法，得到一个整数
0
>>> 17 % 3 # 取余
2
>>> 2 ** 5 # 乘方
32
```

&emsp;&emsp;**注意：**

- `Python` 可以同时为多个变量赋值，如 `a, b = 1, 2`；
- 一个变量可以通过赋值指向不同类型的对象；
- 数值的除法包含两个运算符：`/` 返回一个浮点数，`// `返回一个整数；
- 在混合计算时，`Python` 会把整型转换成为浮点数。

**数值类型实例**

| int | float | complex |
| --- | --- | --- |
| 10 | 0.0 | 3.14j |
| 100 | 15.20 | 45.j |
| -786 | -21.9 | 9.322e-36j | 
| 080 | 32.3e+18 | .876j |
| -0490 | -90 | -.6545+0J |
| -0x260 | -32.54e100 | 3e+26j |
| 0x69 | 70.2E-12 | 4.53e-7j |

&emsp;&emsp;`Python` 还支持复数，复数由实数部分和虚数部分构成，可以用 `a + bj`,或者 `complex(a,b)` 表示， 复数的实部 `a` 和虚部 `b` 都是浮点型。

---

## String(字符串)

&emsp;&emsp;`Python` 中的字符串用单引号 `' `或双引号 `"` 括起来，同时使用反斜杠 `\` 转义特殊字符。

&emsp;&emsp;字符串的截取的语法格式如下：

>变量[头下标:尾下标]

&emsp;&emsp;索引值以 `0` 为开始值，`-1` 为从末尾的开始位置。


&emsp;&emsp;加号 `+` 是字符串的连接符， 星号 `*` 表示复制当前字符串，与之结合的数字为复制的次数。实例如下：

```pyton
str = 'Waffle'

print (str)          # 输出字符串
print (str[0:-1])    # 输出第一个到倒数第二个的所有字符
print (str[0])       # 输出字符串第一个字符
print (str[2:5])     # 输出从第三个开始到第五个的字符
print (str[2:])      # 输出从第三个开始的后的所有字符
print (str * 2)      # 输出字符串两次，也可以写成 print (2 * str)
print (str + "TEST") # 连接字符串
```

&emsp;&emsp;执行以上程序会输出如下结果：

<center><img src="assets/image/3.png" width="500"/></center>

&emsp;&emsp;`Python` 使用反斜杠 `\` 转义特殊字符，如果你不想让反斜杠发生转义，可以在字符串前面添加一个 `r`，表示原始字符串：

```shell
>>> print('Wa\nffle')
Wa
ffle
>>> print(r'Wa\nffle')
Wa\nffle
>>>
```

&emsp;&emsp;另外，反斜杠 `(\)` 可以作为续行符，表示下一行是上一行的延续。也可以使用 `"""..."""` 或者 `'''...'''` 跨越多行。

&emsp;&emsp;注意，`Python` 没有单独的字符类型，一个字符就是长度为 `1` 的字符串。 

```shell
>>> word = 'Python'
>>> print(word[0], word[5])
P n
>>> print(word[-1], word[-6])
n P
```

>注意：与 `C` 字符串不同的是，`Python` 字符串不能被改变。向一个索引位置赋值，比如 `word[0] = 'm'` 会导致错误。 

---

## List(列表)

&emsp;&emsp;`List`（列表） 是 `Python` 中使用最频繁的数据类型。

&emsp;&emsp;列表可以完成大多数集合类的数据结构实现。列表中元素的类型可以不相同，它支持数字，字符串甚至可以包含列表（所谓嵌套）。

&emsp;&emsp;列表是写在方括号 `[]` 之间、用逗号分隔开的元素列表。

&emsp;&emsp;和字符串一样，列表同样可以被索引和截取，列表被截取后返回一个包含所需元素的新列表。

&emsp;&emsp;列表截取的语法格式如下：

```python
变量[头下标:尾下标]
```

&emsp;&emsp;索引值以 `0` 为开始值，`-1` 为从末尾的开始位置。

<center><img src="assets/image/4.png" width="500"/></center>

&emsp;&emsp;加号 `+` 是列表连接运算符，星号 `*` 是重复操作。如下实例： 

```python
list = [ 'abcd', 786 , 2.23, 'Waffle', 70.2 ]
tinylist = [123, 'Nano']

print (list)            # 输出完整列表
print (list[0])         # 输出列表第一个元素
print (list[1:3])       # 从第二个开始输出到第三个元素
print (list[2:])        # 输出从第三个元素开始的所有元素
print (tinylist * 2)    # 输出两次列表
print (list + tinylist) # 连接列表
```

以上实例输出结果：

<center><img src="assets/image/5.png" width="500"/></center>


&emsp;&emsp;与 `Python` 字符串不一样的是，列表中的元素是可以改变的：

```shell
>>> a = [1, 2, 3, 4, 5, 6]
>>> a[0] = 9
>>> a[2:5] = [13, 14, 15]
>>> a
[9, 2, 13, 14, 15, 6]
>>> a[2:5] = []   # 将对应的元素值设置为 []
>>> a
[9, 2, 6]
```

&emsp;&emsp;`List` 内置了有很多方法，例如 `append()`、`pop()` 等等，这在后面会讲到。

&emsp;&emsp;**注意：**
- `List` 写在方括号之间，元素用逗号隔开。
- 和字符串一样，`list` 可以被索引和切片。
- `List` 可以使用 `+` 操作符进行拼接。
- `List` 中的元素是可以改变的。

&emsp;&emsp;`Python` 列表截取可以接收第三个参数，参数作用是截取的步长，以下实例在索引 `1` 到索引 `4` 的位置并设置为步长为 `2`（间隔一个位置）来截取字符串:

```shell
>>>L = ['W','a','f','f','l','e']
>>>L[1:4:2]
['a','f','e']
```

&emsp;&emsp;如果第三个参数为 **负数** 表示 **逆向** 读取，以下实例用于翻转字符串：

```python
def reverseWords(input):
     
    # 通过空格将字符串分隔符，把各个单词分隔为列表
    inputWords = input.split(" ")
 
    # 翻转字符串
    # 假设列表 list = [1,2,3,4],  
    # list[0]=1, list[1]=2 ，而 -1 表示最后一个元素 list[-1]=4 ( 与 list[3]=4 一样)
    # inputWords[-1::-1] 有三个参数或者input[::-1]效果一样
    # 第一个参数 -1 表示最后一个元素
    # 第二个参数为空，表示移动到列表末尾
    # 第三个参数为步长，-1 表示逆向
    inputWords=inputWords[-1::-1]
 
    # 重新组合字符串
    output = ' '.join(inputWords)
     
    return output
 
if __name__ == "__main__":
    input = 'I like Waffle Nano'
    rw = reverseWords(input)
    print(rw)
```

输出结果为：

>Nano Waffle like I

---

## Tuple(元组)

&emsp;&emsp;元组 `（tuple）` 与列表类似，不同之处在于元组的元素不能修改。元组写在小括号 `()` 里，元素之间用逗号隔开。

&emsp;&emsp;元组中的元素类型也可以不相同： 

```python
tuple = ( 'abcd', 786 , 2.23, 'Waffle', 70.2  )
tinytuple = (123, 'Nano')

print (tuple)             # 输出完整元组
print (tuple[0])          # 输出元组的第一个元素
print (tuple[1:3])        # 输出从第二个元素开始到第三个元素
print (tuple[2:])         # 输出从第三个元素开始的所有元素
print (tinytuple * 2)     # 输出两次元组
print (tuple + tinytuple) # 连接元组
```

以上实例输出结果：

<center><img src="assets/image/6.png" width="500"/></center>

&emsp;&emsp;元组与字符串类似，可以被索引且下标索引从 `0` 开始， `-1` 为从末尾开始的位置。也可以进行截取（看上面，这里不再赘述）。

&emsp;&emsp;其实，可以把字符串看作一种特殊的元组。

```shell
>>> tup = (1, 2, 3, 4, 5, 6)
>>> print(tup[0])
1
>>> print(tup[1:5])
(2, 3, 4, 5)
>>> tup[0] = 11  # 修改元组元素的操作是非法的
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: 'tuple' object does not support item assignment
>>>
```

&emsp;&emsp;虽然 `tuple` 的元素不可改变，但它可以包含可变的对象，比如 `list` 列表。

构造包含 `0` 个或 `1` 个元素的元组比较特殊，所以有一些额外的语法规则： 

```python
tup1 = ()    # 空元组
tup2 = (20,) # 一个元素，需要在元素后添加逗号
```

&emsp;&emsp;`string`、`list` 和 `tuple` 都属于 `sequence`（序列）。

&emsp;&emsp;**注意：**
- 与字符串一样，元组的元素不能修改。
- 元组也可以被索引和切片，方法一样。
- 注意构造包含 `0` 或 `1` 个元素的元组的特殊语法规则。
- 元组也可以使用 `+` 操作符进行拼接。

---

## Set(集合)

&emsp;&emsp;集合 `（set）` 是由一个或数个形态各异的大小整体组成的，构成集合的事物或对象称作 **元素** 或是 **成员**。

&emsp;&emsp;基本功能是 **进行成员关系测试** 和 **删除重复元素**。

&emsp;&emsp;可以使用大括号 `{ }` 或者 `set()` 函数创建集合，注意：创建一个空集合必须用 `set()` 而不是 `{ }`，因为 `{ }` 是用来创建一个空字典。

创建格式：

```python
parame = {value01,value02,...}
# 或者
set(value)
```
```python
sites = {'Waffle', 'Nano', 'Micro', 'Labs'}

print(sites)   # 输出集合，重复的元素被自动去掉

# 成员测试
if 'Waffle' in sites :
    print('Waffle 在集合中')
else :
    print('Waffle 不在集合中')


# set可以进行集合运算
a = set('abracadabra')
b = set('alacazam')

print(a)

print(a - b)     # a 和 b 的差集

print(a | b)     # a 和 b 的并集

print(a & b)     # a 和 b 的交集

print(a ^ b)     # a 和 b 中不同时存在的元素
```

以上实例输出结果：

<center><img src="assets/image/7.png" width="500"/></center>

---

## Dictionary(字典)

&emsp;&emsp;字典 `（dictionary）` 是 `Python` 中另一个非常有用的内置数据类型。

&emsp;&emsp;列表是有序的对象集合，字典是无序的对象集合。两者之间的区别在于：字典当中的元素是通过键来存取的，而不是通过偏移存取。

&emsp;&emsp;字典是一种映射类型，字典用 `{ }` 标识，它是一个无序的 键`(key)` : 值`(value)` 的集合。

&emsp;&emsp;键 `(key)` 必须使用不可变类型。

&emsp;&emsp;在同一个字典中，键 `(key)` 必须是唯一的。 

```python
dict = {}
dict['one'] = "1 - WaffleNano"
dict[2]     = "2 - Waffle"

tinydict = {'name': 'Waffle','code':1, 'site': 'https://gitee.com/paniford/waffle_nano_v1_python_api_document'}


print (dict['one'])       # 输出键为 'one' 的值
print (dict[2])           # 输出键为 2 的值
print (tinydict)          # 输出完整的字典
print (tinydict.keys())   # 输出所有键
print (tinydict.values()) # 输出所有值
```

以上实例输出结果：

<center><img src="assets/image/8.png" width="1000"/></center>

&emsp;&emsp;构造函数 `dict()` 可以直接从键值对序列中构建字典如下：

```python
>>> dict([('Waffle', 1), ('blackwalnut', 2), ('Taobao', 3)])
{'Waffle': 1, 'blackwalnut': 2, 'Taobao': 3}
>>> {x: x**2 for x in (2, 4, 6)}
{2: 4, 4: 16, 6: 36}
>>> dict(WaffleNano=1, blackwalnut=2, Taobao=3)
{'WaffleNano': 1, 'blackwalnut': 2, 'Taobao': 3}
>>>
```

&emsp;&emsp;另外，字典类型也有一些内置的函数，例如 `clear()`、`keys()`、`values()` 等。

&emsp;&emsp;**注意：**
- 字典是一种映射类型，它的元素是键值对。
- 字典的关键字必须为不可变类型，且不能重复。
- 创建空字典使用 `{ }`。

---

## Python数据类型转换

&emsp;&emsp;有时候，我们需要对数据内置的类型进行转换，数据类型的转换，你只需要将数据类型作为函数名即可。

&emsp;&emsp;以下几个内置的函数可以执行数据类型之间的转换。这些函数返回一个新的对象，表示转换的值。

| 函数 | 描述 |
| --- | --- |
| int(x[,base]) | 将 x 转换为一个整数。 |
| float(x) | 将 x 转换到一个浮点数。 |
| complex(real[,imag]) | 创建一个复数。 |
| str(x) | 将对象 x 转换为字符串。 |
| repr(x) | 将对象 x 转换为表达式字符串。 |
| eval(str) | 用来计算在字符串中的有效Python表达式,并返回一个对象。 |
| tuple(s) | 将序列 s 转换为一个元组。 |
| list(s) | 将序列 s 转换为一个列表。 |
| set(s) | 转换为可变集合。 |
| dict(d) | 创建一个字典。d 必须是一个 (key, value)元组序列。 |
| frozenset(s) | 转换为不可变集合。 |
| chr(x) | 将一个整数转换为一个字符。 |
| ord(x) | 将一个字符转换为它的整数值。 |
| hex(x) | 将一个整数转换为一个十六进制字符串。 |
| oct(x) | 将一个整数转换为一个八进制字符串。 |




