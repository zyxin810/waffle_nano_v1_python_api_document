# gc – 控制垃圾回收器

该模块实现 `Python 运行时` 的内存管理。

## 函数

### gc.enable()

启用垃圾自动收集。

### gc.disable()

禁用垃圾自动收集。堆内存仍可分配，仍可使用 `gc.collect()` 手动启动垃圾回收。

### gc.collect()

运行垃圾回收。

### gc.mem_alloc()

返回已经分配的堆`RAM`的字节数。

### gc.mem_free()

返回可用的堆`RAM`的字节数。
